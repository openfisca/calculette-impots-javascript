# Calculette Impôts traduite en JavaScript

Démo : http://calc.ir.openfisca.fr/mtojs/

## Installation

Il est nécessaire d'avoir au préalable installé le paquet [calculette-impots-vector-computing](https://git.framasoft.org/openfisca/calculette-impots-vector-computing).

## Objectif souhaité

Transpiler le code M en javascript
Pouvoir faire fonctionner le code M sur navigateur.

## Utilisation

Pour lancer le serveur

```
$ python -m SimpleHTTPServer

```
Ouvrez votre navigateur à localhost:8000
Entrez vos informations - soumettez le calcul

## Membres projet

- Emmanuel
- Mori
